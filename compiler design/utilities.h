#include<stdio.h>
#include<stdlib.h>
#include<string.h>
struct variable 
{
	char name[10];
	int address;
	int size;
};
struct block
{
	char name[10];
	int lineno;
};
struct intermediateCode
{
	int lineno;
	int opcode;
	int parameters[4];
};
int strtoint(char *str);
int allocate_memoryV(char * variable, int tableno, struct variable table[], int address, int size);
int allocate_memoryL(char * label, int tableno, struct block table[], int lineno);
int getOpcode(char *pneumonics, int flag);
int getRegisterCode(char *registername);
int getVariable(char *name, struct variable table[]);
int getlineNo(char *name, struct block table[]);
char *removecommas(char *record);
int isVariable(char *c);
int getSize(char *c);
char *getVariblefromArray(char *record);
int isLabel(char *label);
char *removeTabSpaces(char *record);
char *removecolon(char *label);