
#include"utilities.h"
#include "filereader.h"

int main()
{
	format();
	copy_to_disk("input.txt", "input.txt");
	copy_from_disk("input.txt", "in.txt");
	
	FILE *fptr = fopen("input.txt", "r");
	FILE *fptr2 = fopen("output.txt", "w");

	if (fptr == NULL)
	{
		printf("file not found");
		return -1;
	}
	int address = 0;
	char buffer[1024];
	char *line;
	int flag = 0;
	int tablenoV = 0, tablenoL = 0;
	struct variable symbolTable[26];
	struct block table[26];
	struct intermediateCode ic[1000];
	int lineno = 1;
	
	int *memory = (int*)malloc(sizeof(int) * 100000);
	int stack[100];
	int top = -1;
	while ((line = fgets(buffer, 1024, fptr)) != NULL)
	{
		char *record[4];
		//tokenising
		//dividing each instruction into maximum of 4 parts 
		record[0] = strtok(line, " ");
		record[0] = removeTabSpaces(record[0]);
		record[0] = removecommas(record[0]);
		record[1] = strtok(NULL, " ");
		record[1]=removecommas(record[1]);
		record[2]=record[2] = strtok(NULL, " ");
		record[2]=removecommas(record[2]);
		record[3] = strtok(NULL, " ");
		record[3]=removecommas(record[3]);
		//instrcution is DATA
		if (strcmp(record[0], "DATA") == 0)
		{
			if (isVariable(record[1]))
			{
				tablenoV=allocate_memoryV(record[1], tablenoV, symbolTable, address, 1);
				address += 1;
			}
			else
			{
				int size = getSize(record[1]);
				record[1] = getVariblefromArray(record[1]);
				tablenoV = allocate_memoryV(record[1], tablenoV, symbolTable, address, size);
				address += 4;
			}
		}
		//instruction is constant
		else if (strcmp(record[0], "CONST") == 0)
		{
			
			tablenoV=allocate_memoryV(record[1], tablenoV, symbolTable, address, 0);
			memory[address] = strtoint(record[3]);
			address += 1;
			
			
		}
		else
		{
			//if instruction is MOVE
			if (strcmp(record[0], "MOV") == 0)
			{
				int size1 = getSize(record[1]);
				int size2 = getSize(record[2]);
				record[1] = getVariblefromArray(record[1]);
				record[2] = getVariblefromArray(record[2]);
				int val = getRegisterCode(record[1]);
				if (val != -1)
				{
					ic[lineno].lineno = lineno;
					ic[lineno].opcode = getOpcode(record[0], 1);
					ic[lineno].parameters[0] = val;
					ic[lineno].parameters[1] =getVariable(record[2],symbolTable)+size2;
					ic[lineno].parameters[2] = -1;
					ic[lineno].parameters[3] = -1;
					lineno++;
				

				}
				else
				{
					ic[lineno].lineno = lineno;
					ic[lineno].opcode = getOpcode(record[0], 0);
					ic[lineno].parameters[0] = getVariable(record[1], symbolTable)+size1;
					ic[lineno].parameters[1] = getRegisterCode(record[2]);
					ic[lineno].parameters[2] = -1;
					ic[lineno].parameters[3] = -1;
					lineno++;
				
				}
			}
			//instruction is PRINT
			else if (strcmp(record[0], "PRINT")==0)
			{
				ic[lineno].lineno = lineno;
				int val;
				ic[lineno].opcode = getOpcode("PRINT", 0);
				int size = getSize(record[1]);
				int flag = 0;
				if (!isVariable(record[1]))

				{
					record[1] = getVariblefromArray(record[1]);
					flag = -1;
				}
				val = getVariable(record[1],symbolTable);
				if (flag == -1)
					val += size;
				ic[lineno].parameters[0] = val;
				ic[lineno].parameters[1] = -1;
				ic[lineno].parameters[2] = -1;
				ic[lineno].parameters[3] = -1;
				lineno++;
			

			}
			//if instruction is READ
			else if (strcmp(record[0], "READ")==0)
			{
				ic[lineno].lineno = lineno;
				ic[lineno].opcode = getOpcode("READ", 0);
				ic[lineno].parameters[0] = getRegisterCode(record[1]);
				ic[lineno].parameters[1] = -1;
				ic[lineno].parameters[2] = -1;
				ic[lineno].parameters[3] = -1;
				lineno++;
	

			}
			// iif it is label
			else if (isLabel(record[0]))
			{
				record[0] = removecolon(record[0]);
				if (strcmp(record[0], "START") != 0)
				{
					record[0] = removecommas(record[0]);
					tablenoL = allocate_memoryL(record[0], tablenoL, table, lineno);
				}
			}
			//instruction is IF
			else if (strcmp(record[0],"IF")==0)
			{
				ic[lineno].lineno = lineno;
				ic[lineno].opcode = getOpcode("IF", 0);
				ic[lineno].parameters[0] = getRegisterCode(record[1]);
				ic[lineno].parameters[1] = getRegisterCode(record[3]);
				ic[lineno].parameters[2] = getOpcode(record[2], 0);
				stack[++top] = lineno;
				tablenoL = allocate_memoryL(record[0], tablenoL, table, lineno);
				lineno++;
				

			}
			//if instruction is ELSE
			else if (strcmp(record[0], "ELSE") == 0)
			{
				ic[lineno].lineno = lineno;
				ic[lineno].opcode = getOpcode("JMP", 0);
				ic[lineno].parameters[1] = -1;
				ic[lineno].parameters[2] = -1;
				ic[lineno].parameters[3] = -1;
				stack[++top] = lineno;
				tablenoL = allocate_memoryL(record[0], tablenoL, table, lineno+1);
				lineno++;
			}
			//if instruction is ENDIF
			else if (strcmp(record[0], "ENDIF") == 0)
			{
				
				int opcode = getOpcode("JMP", 0);
				int k = stack[top--];
				if (ic[k].opcode == opcode)
				{
					ic[k].parameters[0] = lineno ;
					int j = stack[top--];
					ic[j].parameters[3] = k + 1;
				}
				else
				{
					ic[k].parameters[3] = lineno;
				}
				tablenoL = allocate_memoryL(record[0], tablenoL, table, lineno);

			}
			// if instruction is JUMP
			else if (strcmp(record[0], "JMP") == 0)
			{
				record[1]=removecolon(record[1]);
				int ln = getlineNo(record[1],table);
				ic[lineno].opcode = getOpcode("JMP", 0);
				ic[lineno].lineno = lineno;
				ic[lineno].parameters[0] = ln;
				ic[lineno].parameters[1] = -1;
				ic[lineno].parameters[2] = -1;
				ic[lineno].parameters[3] = -1;
				lineno++;

			}
			else
			{
				
				int val = getOpcode(record[0], 0);
				if (val != -1)
				{
					ic[lineno].opcode = val;

					ic[lineno].lineno = lineno;
					ic[lineno].parameters[0] = getRegisterCode(record[1]);
					ic[lineno].parameters[1] = getRegisterCode(record[2]);
					ic[lineno].parameters[2] = getRegisterCode(record[3]);
					ic[lineno].parameters[3] = -1;
					lineno++;
				}
			}
		}
	}
	fclose(fptr);

	//write to file
	for (int i = 1; i < lineno; i++)
	{
		fprintf(fptr2,"%d %d ", ic[i].lineno, ic[i].opcode);
		for (int j = 0; j < 4 && ic[i].parameters[j] != -1; j++)
			fprintf(fptr2,"%d ", ic[i].parameters[j]);
		fprintf(fptr2,"\n");
	}
	fclose(fptr2);
	copy_to_disk("output.txt", "out.txt");
	copy_from_disk("out.txt", "o.txt");
	ls();
	readfromfile("out.txt");
	

	
	return 0;
}